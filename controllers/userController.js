const User = require("../models/user");
const Course = require("../models/course");
const auth = require("../auth");

const bcrypt = require("bcrypt");
const course = require("../models/course");
/* 
  1. use mongoose method "find" to find duplicate emails
  2. use .then method to send a response based on the result of the find method
*/

// retrieving all users
module.exports.getAllUsers = () => {
  return User.find({}).then(result => {
      return result;
  })
}; 

// checking existing user/email
module.exports.checkEmailExists = (reqBody) => {
  return User.find({email: reqBody.email}).then(result => {
    if (result.length > 0){
      return "Email already exists.";
    } else {
      return "Email is not in use.";
    }
  })
}; 

// checking existing user/email
module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    mobileNo: reqBody.mobileNo,
    password: bcrypt.hashSync(reqBody.password, 10)
  })
  return newUser.save().then((user, error) =>{
    if (error) {
      return false;
    }else{
      return user;
    }
  })
}

// USER LOGIN
/*
  1. check the database if the user email exist
  2. compare the password provided in the request body with the password stored in the database
  3. generate/return a JSON wen token if the user had successfully logged in and return false if not
*/

module.exports.loginUser = ( reqBody ) => {
  return User.findOne( { email: reqBody.email } ).then(result =>{
      // if the user email does not exist
      if(result === null){
          return false;
      // if the user email exists in the database
      } else {
          // compareSync = decodes the encrypted password from the database and compares it to the password received from the request body
          // it's a good that if the value returned by a method/function is boolean, the variable name should be answerable by yes/no
          const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

          if(isPasswordCorrect){
              return { access: auth.createAccessToken(result) }
          }else{
              return false;
          }
      }
  } )
}

  module.exports.getProfile = (data) => {
    console.log(data);
    return User.findById(data.userId).then(result => {
  
      // Changes the value of the user's password to an empty string when returned to the frontend
      // Not doing so will expose the user's password which will also not be needed in other parts of our application
      // Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database 
      
      // only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;
	});
};

module.exports.enroll = async (data) => {
  let isUserUpdated = await User.findById(data.userId).then(user => {
    user.enrollments.push({courseId: data.courseId});

    return user.save().then((user, error) => {
      if(error){
        return false;
      } else {
        return true;
      }
    })
  });

  let isCourseUpdated = await Course.findById(data.courseId).then(course => {
      course.enrollees.push({userId:data.userId});

      return course.save().then((course, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
      })
    })
    if (isUserUpdated && isCourseUpdated) {
      return true;
    } else {
      return false;
    }
  }

//   Activity:
// 1. Refactor the user route to implement user authentication for the enroll route.
// 2. Process a POST request at the /enroll route using postman to enroll a user to a course.
// 3. Push to git with the commit message of Add activity code - S41.
// 4. Add the link in Boodle.