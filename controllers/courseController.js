const Course = require("../models/course");
const User = require("../models/user");
const bcrypt = require("bcrypt");

// retrieving all courses
module.exports.getAllCourses = () => {
  return Course.find({})
  .then (res => {
    return res; 
  })
};

// retrieving all active courses
module.exports.getAllActive = () => {
  return Course.find({isActive: true})
  .then (result => {
    return result; 
  })
};

// retrieving a specific course
module.exports.getCourse = (reqParams) => {
  return Course.findById(reqParams)
  .then (result => {
    return result; 
  })
};

// creating a course
module.exports.addCourse = (data) => {
	let newCourse = new Course({
		name : data.name,
		description : data.description,
		price : data.price
	});
	return newCourse.save().then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};


/*
	Miniactivity:
		1. Create a variable "updateCourse" which will contain the information from the requestBody
		2. Find and update the course using the courseId found in the request params and the variable "updateCourse" containing the information from the requestBody (findByIdAndUpdate)
		3. return false if there are errors, true if the updating is successful

		10 minutes: 7:56 pm (send the output ss in the google chat)
*/

// updating course
module.exports.updateCourse = ( reqParams, reqBody ) => {
	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	// findByIdAndUpdate - its purpose is to find a specific id in the database (first parameter) and update it using the information coming from the request body (second parameter)
	/*
		findByIdAndUpdate(documentId, updatesToBeApplied)
	*/
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, err)=>{
		if (err) {
			return false;
		}else{
			return true;
		}
	})
};



// archiving inactive course
module.exports.archiveCourse = (reqParams, reqBody) => {
	let updateActiveField = {
		isActive : false
	};
	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};

