const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require ("../auth");


// ** NOTES **
// user "post" if we need req.body
// route for checking if the user's email already exists in the database

//GET ALL USERS
router.get("/", (req, res) => {
  userController.getAllUsers()
  .then(resultFromController => res.send(resultFromController));
});

//CHECK EMAIL EXIST
router.post("/checkEmail", (req, res) => {
  userController.checkEmailExists(req.body)
  .then(resultFromController => res.send(resultFromController));
});

//USER REGISTRATION
router.post("/register", (req, res) => {
  userController.registerUser(req.body)
  .then(resultFromController => res.send(resultFromController));
});

//USER AUTHENTICATION
router.post("/login", (req, res) => {
  userController.loginUser(req.body)
  .then(resultFromController => res.send(resultFromController));
});

//USER DETAILS (WITH ACCESS TOKEN)
router.get("/details", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController.getProfile({userId: userData.id})
  .then(resultFromController => res.send(resultFromController));
});

//USER ENROLLMENT (WITH TOKEN)
// router.post('/enroll', auth.verify, (req, res) => {
//   const userData = auth.decode(req.headers.authorization);
// 	let data = {
// 		userId: req.body.userId,
// 		courseId: req.body.courseId
// 	}
// 	userController.enroll(data).then(resultFromController => res.send(resultFromController));
// } )

router.post('/enroll', auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(resultFromController => res.send(resultFromController));
} )


module.exports = router;

