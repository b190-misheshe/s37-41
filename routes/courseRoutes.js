const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

// Route for creating a course
router.post("/", auth.verify, (req, res) => {
  const userAdmin = auth.decode(req.headers.authorization);
	console.log(userAdmin);
  courseController.addCourse(req.body, userAdmin)
  .then(resultFromController => res.send(resultFromController));
});

// Route for getting all courses
router.get("/all", (req, res) => {
  courseController.getAllCourses(req.body)
  .then(resultFromController => res.send(resultFromController));
});

// Route for getting all ACTIVE courses
router.get("/", (req, res) => {
  courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving a specific course
router.get("/:id", (req, res) => {
  courseController.getCourse(req.params.id)
  .then(resultFromController => res.send(resultFromController));
});









// Route for updating  a specific course
router.put('/:courseId', auth.verify, (req, res) =>{
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
} );


//ACTIVITY:
// 1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
// 2. Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.
// 3. Process a PUT request at the /courseId/archive route using postman to archive a course


// Route for archiving a course
router.put("/:courseId/archive", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	} else {
		courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
	}
});


//=======================================
module.exports = router;