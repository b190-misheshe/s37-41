const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const app = express();

// ** MongoDB Connection
mongoose.connect("mongodb+srv://misheshe:admin@wdc028-course-booking.jj30hwt.mongodb.net/b190-course-booking?retryWrites=true&w=majority",
{
  useNewUrlParser : true,
  useUnifiedTopology : true
}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=> console.log("We're connected to the database"));

// allow all resources to access our backend app
// not best practice (only for dev stage)
// on cap3 there's a better way to do this
app.use(cors());


app.use(express.json());
app.use(express.urlencoded({extended: true}));

// ** ROUTES
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// handles the environment of the hosting websites should app be hosted in a website such as Heroku
app.listen(process.env.PORT || 3000, () => console.log(`API now online at port ${process.env.PORT || 3000}`));