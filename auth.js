// if users are registered, if reg (check if admin/user only)
const jwt = require("jsonwebtoken"); 

// token > secret (with algo for more security purposes); additional security
// added security in auth
const secret = "AngelBabyAngel";

module.exports.createAccessToken = (user) => {
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin
  };
  return jwt.sign(data,secret,{});
}

// Token Verification thru "verify method" - token received and secret
// "in lazada, ?"
module.exports.verify = (req, res, next) => {
  // the token is retrieved from the request header
  // POSTMAN - Authorization - Bearer Toker
  let token = req.headers.authorization

  // "undefined" - declared, but no value 
  if (typeof token !== "undefined"){
    console.log(token);
    // reassignment to token,
    // the token sent is a type of "bearer" token which when received, contains the "bearer" as a prefix to the string
    token = token.slice(7,token.length);
    // validates the token
    return jwt.verify(token, secret, (err, data) => {
      // should the jwt is invalid
      if (err) {
        return res.send({auth: "failed"});
      } else {
        // allows the application to proceed with the next middleware function/callback function in the route
        next();
      }
    })
  } else {
    return res.send({auth: "failed"});
  }
};

// Token Decryption
// "in lazada, is this the correct parcel?"
module.exports.decode = (token) => {
  if (typeof token !== "undefined") {
    token = token.slice(7, token.length);
    return jwt.verify (token, secret, (err, data) => {
      if (err) {
        return null;
      } else {
        return jwt.decode(token, {complete: true}).payload;
      }
    })
  };
};

